class Camper():
	def __init__(self, name, batch, course_type):
		self.name = name
		self.batch = batch
		self.course_type = course_type

	def Career_Track(self):
		print(f"Currently enrolled in the {self.course_type} program")

	def Info(self):
		print(f"My name is {self.name} of batch {self.batch}")

zuitt_camper = Camper("Caesar", 69, "RIOT Internship")

print(f"Camper Name: {zuitt_camper.name}")
print(f"Camper Batch: {zuitt_camper.batch}")
print(f"Camper Course: {zuitt_camper.course_type}")

zuitt_camper.Info()
zuitt_camper.Career_Track()