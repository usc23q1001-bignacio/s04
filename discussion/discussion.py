# Lists
# To create a list, the square bracket is used
names = ["John", "Paul","George","Ringo"]
programs = ["developer career","pi-shape","short courses"]
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]

# A list can contain elements of different data Types
sample_list = ["Apple", 3, False, "Potato", 4, True]
# print(sample_list)

# Getting the list size
# The number of elements in a list can be counted using the len method()
# print(len(programs))

# Accessing values 
# List can be accessed by providing the index number of the element

# Access the first item in the list
# print(programs[0])

# Mini-exercise
students = ["Steve","Mary","John","Larry","Shaun"]
grades = [25, 50, 75, 100, 100]

# for i in range(0,len(students)):
	# print(f"The grade of {students[i]} is {grades[i]}")

# List Manipulation
# List has methods that can be used to manipulate the elements within

# Adding List items - the append() method allows to insert items to a list
# programs.append('global')
# print(programs)

# Deleting list items - the "del" keyword can be used to delete elements in the list
# durations.append(360)
# print(durations)

# Delete last item from list
# del durations[-1]
# print(durations)

# Membership checks
# print(20 in durations)
# print(500 in durations)

# Sorting lists
# print(names)
# names.sort()
# print(names)

# Emptying the list
# test_list = [1, 3, 5, 7]
# print(test_list)
# test_list.clear()
# print(test_list)

# Dictionary
person1 = {
	"name": "Khaleed",
	"age": 18,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

# print(person1)

# number of key-value pairs
# print(len(person1))

# accessing values
# print(print(person1.values()))

# accessing items
# print(print(person1.items()))

# keys
# print(print(person1.keys()))

# Empty the dictioary
# person1.clear()

# Mini-exercise
car = {
	"brand": "Toyota",
	"model": "Avanza",
	"year_of_make": 2022,
	"color": "Silver"
}

# print(f"I own a {car['brand']} {car['model']} and it was made in {car['year_of_make']}")

# Functions
# Functions are blocks of code that runs when called
# The "def" keyword is used to create a function. The syntax is def <functionName>()

# Define a function called my_greeting
def my_greeting():
	# code to be run when my_greeting is called back
	print(f"Hello User")

# Calling/Invoking a function
my_greeting()

# Parameters can be added to function to have more control to what the inputs for the function will be
def greet_user(username = "User"):
	# prints out the value of the username parameter
	print(f"Hello, {username}!")

# Arguments are the values that are substituted to the parameters
greet_user("Bliggy")
greet_user()

# From a function's perspective:
# A parameter is a variable listed inside the parenthesis in the function definition
# Argument is the value that is sent to the function when it is called

# Return statement - the "return" keyword allow functions to return values 
def addition(num1, num2):
	return num1 + num2

print(f"The sum is {addition(5,5)}")

# Lambda functions
# A lambda functions is a small anonymous function that can be used for callbacks

greeting = lambda person = "Smalling" : f'Hello {person}'
print(greeting("Elvis"))
print(greeting("Anthony"))
print(greeting())

multiply = lambda a, b : a * b
print(multiply(5,6)) 
print(multiply(6,99))

# Classes
# Are blueprints to describe the concepts of objects
# To create a class, the "class" keyword is used along with the class name that starts with an uppercase letter
# class ClassName()

class Car():
	# Properties
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make
	# Other properties can be added and assigned hard-coded values
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# Methods
	def fill_fuel(self):
		print(f'Current fuel level: {self.fuel_level}')
		print(f'Filling up the fuel tank ...')
		self.fuel_level = 100
		print(f'New fuel level: {self.fuel_level}')

	def drive(self, distance):
		print(f"The car is driven {distance} kilometers.")
		print(f"The fuel level left: {self.fuel_level - distance}")
		self.fuel_level = self.fuel_level - distance

# Creating a new instance is done by calling the class and providing the arguments

new_car = Car("Nissan", "GTX 2080Ti", "2019")

# Displaying attributes can be done using the dot notation
print(f'My car is a {new_car.brand} {new_car.model}')

# Calling methods of the instance
new_car.fill_fuel()
new_car.drive(50)
new_car.drive(50)

